from __future__ import unicode_literals

from django.conf.urls import url


from .views import (
    AnalyzerListView, AnalyzerCreateView, AnalyzerEditView,
    AnalyzerDeleteView, DocumentVersionResultView, DocumentResultView,
    SetupAnalyzerDocumentTypesView, DocumentSubmitView, DocumentAllSubmitView
)


urlpatterns = [
    url(
        r'^analyzer/(?P<pk>\d+)/submit/$', DocumentSubmitView.as_view(),
        name='document_submit'
    ),
    url(
        r'^documents/multiple/submit/$', DocumentSubmitView.as_view(),
        name='document_submit_multiple'
    ),
    url(
        r'^analyzer/all/submit/$', DocumentAllSubmitView.as_view(),
        name='document_submit_all'
    ),
    url(
        r'^analyzer/list/$', AnalyzerListView.as_view(),
        name='analyzer_list'
    ),
    url(
        r'^analyzer/create/$', AnalyzerCreateView.as_view(),
        name='analyzer_create'
    ),
    url(
        r'^analyzer/(?P<pk>\d+)/edit/$', AnalyzerEditView.as_view(),
        name='analyzer_edit'
    ),
    url(
        r'^analyzer/(?P<pk>\d+)/delete/$', AnalyzerDeleteView.as_view(),
        name='analyzer_delete'
    ),
    url(
        r'^document/version/(?P<pk>\d+)/result/$',
        DocumentVersionResultView.as_view(),
        name='analyzer_document_version_result'
    ),
    url(
        r'^document/(?P<pk>\d+)/result/$',
        DocumentResultView.as_view(),
        name='analyzer_document_result'
    ),
    url(
        r'^setup/(?P<pk>\d+)/analyzer_document_types/$',
        SetupAnalyzerDocumentTypesView.as_view(),
        name='setup_analyzer_document_types'
    ),
]
