from __future__ import unicode_literals

from datetime import timedelta

from common import (
    MayanAppConfig, menu_facet, menu_multi_item, menu_object,
    menu_secondary, menu_setup, menu_tools
)
from common.settings import settings_db_sync_task_delay
from django.apps import apps
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from navigation import SourceColumn
from ocr.signals import post_document_version_ocr

from .classes import DocumentAnalyzeResultHelper, DocumentVersionAnalyzeResultHelper
from .handlers import analyze_document
from .links import (
    link_analyzer_list, link_analyzer_create, link_analyzer_delete,
    link_analyzer_edit, link_document_submit, link_analyzer_document_types,
    link_document_submit_all, link_document_submit_multiple,
    link_analyzer_document_version_result, link_analyzer_document_result
)


def document_analyze_submit(self):
    latest_version = self.latest_version
    # Don't error out if document has no version
    if latest_version:
        latest_version.submit_for_analyze()


def document_version_analyze_submit(self):
    from .tasks import task_do_analyze

    task_do_analyze.apply_async(
        eta=now() + timedelta(seconds=settings_db_sync_task_delay.value),
        kwargs={'document_version_pk': self.pk},
    )


class DocumentAnalyzerApp(MayanAppConfig):
    name = 'document_analyzer'
    test = True
    verbose_name = _('Document Analyzer')

    def ready(self):
        super(DocumentAnalyzerApp, self).ready()

        Analyzer = self.get_model('Analyzer')
        Result = self.get_model('Result')

        Document = apps.get_model(
            app_label='documents', model_name='Document'
        )

        DocumentVersion = apps.get_model(
            app_label='documents', model_name='DocumentVersion'
        )

        Document.add_to_class(
            'analyzer_value_of', DocumentAnalyzeResultHelper.constructor
        )

        DocumentVersion.add_to_class(
            'analyzer_value_of', DocumentVersionAnalyzeResultHelper.constructor
        )

        Document.add_to_class('submit_for_analyze', document_analyze_submit)
        DocumentVersion.add_to_class(
            'submit_for_analyze', document_version_analyze_submit
        )

        SourceColumn(
            source=Analyzer, label=_('Label'),
            attribute='label'
        )

        SourceColumn(
            source=Analyzer, label=_('Slug'),
            attribute='slug'
        )

        SourceColumn(
            source=Analyzer, label=_('Type'),
            attribute='type'
        )

        SourceColumn(
            source=Analyzer, label=_('Parameter'),
            attribute='parameter'
        )

        SourceColumn(
            source=Result, label=_('Analyzer'),
            attribute='analyzer'
        )

        SourceColumn(
            source=Result, label=_('Parameter'),
            attribute='parameter'
        )

        SourceColumn(
            source=Result, label=_('Value'),
            attribute='value'
        )

        menu_facet.bind_links(
            links=(link_analyzer_document_result,),
            sources=(Document,)
        )

        # Multiple Select menue
        menu_multi_item.bind_links(
            links=(link_document_submit_multiple,), sources=(Document,)
        )

        # Action menu of the version in the version screen
        menu_object.bind_links(
            links=(link_analyzer_document_version_result,),
            sources=(DocumentVersion,)
        )

        # bind to Action-Menue
        menu_object.bind_links(
            links=(link_document_submit,),
            sources=(Document,)
        )

        # Setup menu
        menu_setup.bind_links(links=(link_analyzer_list,))

        # Setup: bind to right menu of analyzer list
        menu_secondary.bind_links(
            links=(link_analyzer_list, link_analyzer_create),
            sources=(
                Analyzer, 'document_analyzer:analyzer_create',
                'document_analyzer:analyzer_list'
            )
        )

        # Setup: bind to analyzer list
        menu_object.bind_links(
            links=(link_analyzer_delete, link_analyzer_edit, link_analyzer_document_types),
            sources=(Analyzer,)
        )

        # Tools menu
        menu_tools.bind_links(
            links=(
                link_document_submit_all,  # link_document_type_submit,
            )
        )

        post_document_version_ocr.connect(
            analyze_document, dispatch_uid='analyze_document',
            sender=None
            # sender=task_do_ocr
        )
